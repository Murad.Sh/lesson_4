package com.shukur.lesson4.Task2;

import java.util.ArrayList;
import java.util.List;

public class Task2 {
    public List<Long> castingToLongType(List<String> list) {
        List<Long> newList = list.stream().filter(element -> {
            try {Long.parseLong(element);
            return true;
            } catch (NumberFormatException e) {
            return false;
            }
        }).map(Long::parseLong)
                .toList();
        return newList;
    }
}
