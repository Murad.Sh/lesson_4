package com.shukur.lesson4;

import com.shukur.lesson4.Task1.Laptop;
import com.shukur.lesson4.Task1.Printable;
import com.shukur.lesson4.Task2.Task2;
import com.shukur.lesson4.Task3.Task3;
import com.shukur.lesson4.Task5.Logic;
import com.shukur.lesson4.Task5.Person;
import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.config.Task;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Lesson4Application {

    public static void main(String[] args) {
        SpringApplication.run(Lesson4Application.class, args);

        Task2 task2 = new Task2();
        List<String> list = new ArrayList<>();
        list.add("salam");
        list.add("necesen?");
        list.add("23");
        list.add("45");
        list.add("sag ol");
        list.add("82");
        list.add("ok");
        List<Long> list2 = task2.castingToLongType(list);
        System.out.println(list2);

        Task3 task3 = new Task3();
        List<String> sentences = List.of(
                "Java streams are powerful",
                "Streams allow functional programming in Java",
                "Functional programming is powerful"
        );
        List<String> list3 = task3.sentenceToWords(sentences);
        System.out.println(list3);

        Logic task5 = new Logic();
        List<Person> persons = task5.createListOfPersons(20);
        persons.forEach(person -> System.out.println(person.getName() + " - " + person.getAge()));
    }

}
