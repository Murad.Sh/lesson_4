package com.shukur.lesson4.Task5;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Logic {
    private final Random random = new Random();

    public List<String> listOfNames() {
        List<String> sampleNames = new ArrayList<String>();
        sampleNames.add("Murad");
        sampleNames.add("Isgender");
        sampleNames.add("Kenan");
        sampleNames.add("Aysun");
        sampleNames.add("Elmin");
        sampleNames.add("Aygul");
        sampleNames.add("Telman");
        sampleNames.add("Shafig");
        return sampleNames;
    }

    public List<Person> createListOfPersons(int numberOfPersons) {
        List<String> sampleNames = listOfNames();
        return IntStream.range(0, numberOfPersons)
                .mapToObj(i -> new Person(
                        sampleNames.get(random.nextInt(sampleNames.size())),
                        18 + random.nextInt(63)))
                .collect(Collectors.toList());
    }
}
